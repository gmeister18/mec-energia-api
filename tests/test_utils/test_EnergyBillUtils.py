import datetime


class EnergyBillUtils:

    @staticmethod
    def criar_data_de_conta_de_energia(month, year):
        return datetime.date(year, month, 1)

    @staticmethod
    def gerar_datas_para_recomendacao(date):
        energy_bills_list = []

        month = date.month
        year = date.year

        for _ in range(settings.IDEAL_ENERGY_BILLS_FOR_RECOMMENDATION):
            energy_bills_list, month, year = EnergyBillUtils.atualizar_data_e_inserir_conta_de_energia_na_lista(
                energy_bills_list, month, year
            )

        return energy_bills_list

    @staticmethod
    def atualizar_data_e_inserir_conta_de_energia_na_lista(
        energy_bills_list, month, year
    ):
        month -= 1 if month != 1 else 12
        year -= 1 if month == 1 else 0

        date = EnergyBillUtils.criar_data_de_conta_de_energia(month, year)
        energy_bills_list.append(date)

        return energy_bills_list, month, year

    @staticmethod
    def gerar_datas(start_date, end_date):
        dates = {}

        month = end_date.month
        year = end_date.year

        start_date_year = start_date.year

        while (year >= start_date_year):
            dates[str(year)] = []

            for i in range(12, 0, -1):
                date = EnergyBillUtils.criar_data_de_conta_de_energia(month, year)
                dates[str(year)].append(date)

                month -= 1 if month != 1 else 12
                year -= 1 if month == 1 else 0

        return dates
